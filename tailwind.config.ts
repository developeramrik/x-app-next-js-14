import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './src/modules/**/*.{js,ts,jsx,tsx,mdx}',
    './src/@/components/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      screens: {
        xxxs: '320px',
        xxs: '400px',
        xs: '480px',
        xsp: '370px',
        '3xl': '1920px',
        '4xl': '2320px',
        '5xl': '2620px',
        '6xl': '3220px',
      },
      fontSize: {
        '10px': '10px',
        xxs: '11px',
        '22px': '22px',
        '40px': '40px',
      },
      fontFamily: {
        inter: ['Inter'],
        interBold: ['InterBold', 'sans-serif', 'Times New Roman'],
        interRegular: ['InterRegular', 'sans-serif', 'Times New Roman'],
        interMedium: ['InterMedium', 'sans-serif', 'Times New Roman'],
      },
      spacing: {
        '70px': '70px',
        '3/10': '30%',
        '7/10': '70%',
        '3/14': '21.43%',
        '11/14': '78.57%',
        9: '2.25rem',
        14: '3.5rem',
        68: '17rem',
      },
      minWidth: {
        '1/2': '50%',
      },
      maxWidth: {
        60: '240px',
        64: '260px',
        76: '300px',
        80: '320px',
        84: '360px',
        550: '550px',
      },
      colors: {
        primary: '#7262D9',
        accent: '#7987FF',
        content: '#ACACAC',
        'content-light': '#A6A6A6',
        gray: '#EEEEEE',
        'gray-2': '#D9D9D9',
        destructive: '#F4002C',
        'placeholder-border-color': '#444444',
        'onboarding-bg': '#060606',
        'coming-soon': 'rgba(84, 63, 217, 0.28)',
        'text-new-Title': '#FFF',
        'text-new-Body': '#AFAFAF',
      },
      borderRadius: {
        '10px': '10px',
        '20px': '20px',
        '30px': '30px',
      },
      borderWidth: {
        3: '3px',
      },
      boxShadow: {
        accentshadow: '0px 0px 10px 2px rgba(121, 159, 255, 0.5)',
        'accentshadow-sm': '0px 0px 7px 1px rgba(121, 159, 255, 0.5)',
        statsshadow:
          'inset 3.04px -3.04px 3.04px rgba(155, 178, 214, 0.344), inset -3.04px 3.04px 3.04px rgba(255, 255, 255, 0.344)',
        ghostshadow:
          'inset 3.04px -3.04px 3.04px rgba(155, 178, 214, 0.344), inset -3.04px 3.04px 3.04px rgba(255, 255, 255, 0.344);',
      },
      backgroundColor: {
        'button-gradient': 'linear(153.06deg, #83EAF1 -0.31%, #63A4FF 99.69%)',
        'text-gradient': 'linear(153.06deg, #93FB9D -0.31%, #09C7FB 99.69%)',
        content: '#EAFAFFB2',
        'accent-gradient':
          'linear(167.08deg, #4A4CE3 2.85%, rgba(111, 112, 159, 0.58) 100.8%)',
        'placeholder-color': '#212121',
      },
      background: {
        'auth-new-gradient':
          'linear-gradient(180deg, #4E419B -0.61%, #574BA3 46.39%, #7264C9 99.51%)',

        'green-gradient':
          'linear-gradient(153.06deg, #93FB9D -0.31%, #09C7FB 99.69%)',
        'line-gradient':
          'linear-gradient(270deg, #84F5A8 -1.6%, rgba(37, 210, 232, 0.19) 100%)',
        'solid-button-gradient':
          'linear-gradient(102.3deg, rgba(79, 12, 238, 0.65) 17.62%, rgba(20, 77, 232, 0.65) 100.5%), linear-gradient(180deg, #7F51EC 0%, rgba(127, 81, 236, 0) 100%), linear-gradient(180deg, rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0) 100%);',
      },
      backgroundImage: {
        // 'gradient-radial': 'radial-gradient(50% 50% at 50% 55%, var(--tw-gradient-stops))',
        'auth-img': "url('/static/images/bg-images/auth-bg.svg')",
        'auth-footer': "url('/static/images/bg-images/auth-footer.svg')",
        'auth-new-gradient':
          'linear-gradient(180deg, #4E419B -0.61%, #574BA3 46.39%, #7264C9 99.51%)',
        'gradient-radial':
          'radial-gradient(ellipse 80% 60%, var(--tw-gradient-stops))',
        'solid-button-gradient':
          'linear-gradient(102.3deg, rgba(79, 12, 238, 0.65) 17.62%, rgba(20, 77, 232, 0.65) 100.5%), linear-gradient(180deg, #7F51EC 0%, rgba(127, 81, 236, 0) 100%), linear-gradient(180deg, rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0) 100%);',
      },
      blur: {
        100: '100px',
        50: '50px',
        75: '75px',
        150: '150px',
        175: '175px',
      },
      animation: {
        marquee: 'marquee 10s linear infinite',
        marquee2: 'marquee2 10s linear infinite',
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
      },

      keyframes: {
        marquee: {
          '0%': { transform: 'translateX(0%)' },
          '100%': { transform: 'translateX(-100%)' },
        },
        marquee2: {
          '0%': { transform: 'translateX(100%)' },
          '100%': { transform: 'translateX(0%)' },
        },
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' },
        },
      },
    },
  },
  variants: {
    visibility: ['responsive', 'first', 'hover', 'focus'],
  },
  plugins: [
    require('tailwindcss-animate'),
    require('tailwind-scrollbar-hide'),
    require('tailwind-typewriter')({
      wordsets: {
        heading: {
          words: [
            'invest in top startups',
            'sell your ESOPs',
            'sell your shares',
            'create buyback events ',
          ],
          delay: 2,
        },
      },
    }),
  ],
}
export default config
