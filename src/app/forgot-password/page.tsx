"use client";
import { Button } from '@/components/ui/button';
import {
    Form,
    FormControl,
    FormField,
    FormItem,
    FormLabel,
    FormMessage
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { AppLayout } from '@components/index';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useForm } from "react-hook-form";

const ForgotPassword = () => {
    const schema = Joi.object({
        email: Joi.string().email({ tlds: { allow: false } }).required(),
    });

    const form = useForm({
        resolver: joiResolver(schema)
    });

    const router = useRouter();
    //@ts-ignore
    const onSubmit = async (data) => {
        try {
            await form.handleSubmit(async (values) => {
                // Pass the email to the verify-otp page
                console.log(values, "submitted");
                router.push(`/verify-otp?email=${values.email}`);
            })(data);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <>
            <AppLayout type='auth'>
                <div className='flex flex-col w-3/4'>
                    <h1 className='text-xl xl:text-2xl font-semibold text-gray'>
                        Forgot Password?
                    </h1>
                    <p className='text-gray-2 text-base'>
                        Enter the email associated with your account to change your password.
                    </p>
                    <Form {...form}>
                        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8 mt-4">
                            <FormField
                                control={form.control}
                                name="email"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel className="text-gray">Email</FormLabel>
                                        <FormControl>
                                            <Input placeholder="Enter your email address" {...field} className="font-interRegular" />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />

                            <Button type="button" size="lg" className="w-full" onClick={onSubmit}>Send OTP</Button>
                            
                            <div className="flex w-full">
                                <Link href='/'>
                                    <span className='text-[#9383FD] ml-2 cursor-pointer underline '>Back to Login</span>
                                </Link>
                            </div>
                        </form>
                    </Form>
                </div>
            </AppLayout>
        </>
    )
}
export default ForgotPassword;