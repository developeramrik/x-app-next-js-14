"use client";
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { useForm } from "react-hook-form"
import Joi from 'joi';
import { joiResolver } from '@hookform/resolvers/joi';
import { Button } from '@/components/ui/button'
import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
function SignInForm() {
    // ...
    const schema = Joi.object({
        email: Joi.string().email({ tlds: { allow: false } }).required(),
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    })
    const form = useForm({
        resolver: joiResolver(schema)
    })
    const router = useRouter()
    function onSubmit(values: any) {
        console.log(values)
    }
    return (
        <>
            <Form  {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
                    <FormField
                        control={form.control}
                        name="Email"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel className="text-gray">Email</FormLabel>
                                <FormControl>
                                    <Input placeholder="Enter Email" {...field} className="font-interRegular" />
                                </FormControl>
                                <FormDescription>
                                    This is your public display name.
                                </FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="password"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel className="text-gray">Password</FormLabel>
                                <FormControl>
                                    <>
                                        <Input placeholder="password" {...field} className="font-interRegular" variant="password" type="password" />

                                    </>
                                </FormControl>
                                <FormMessage />
                            </FormItem>
                        )}
                    />
                    <div className=" flex w-full justify-end">
                        <Link href='/forgot-password' className="underline-offset-4 "> <span className='text-[#9383FD] ml-2 cursor-pointer hover:underline '>Forgot Password?</span></Link>
                    </div>
                    <Button size="lg" className="w-full" onClick={() => {
                        router.push('/dashboard')
                    }}>Login</Button>
                </form>
            </Form>
        </>
    )
}
export { SignInForm };
