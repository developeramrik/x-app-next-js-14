"use client"
import { signinUser } from '@auth/api/auth'
import { ErrorType } from '@auth/interfaces'
import { useToast } from '@chakra-ui/react'
import { AppLayout, OpenGraph } from '@components/index'
import EmailContext, { EmailProvider } from '@context/email'
import { IsUSDProvider } from '@context/isUSD'
import { PhoneProvider } from '@context/phone'
import { useMutation } from '@tanstack/react-query'
import { INCENTIV_URL } from '@utils/constants'
import storage from '@utils/storage/storage'
import Image from "next/legacy/image"
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation'
import { ChangeEvent, FormEvent, useContext, useEffect, useState } from 'react'
import '../styles/globals.css'
import dynamic from 'next/dynamic'
const SignInForm = dynamic(() => import('./(signin)/signInForm').then((mod) => mod.SignInForm))

const Page = () => {
  const { email } = useContext(EmailContext)
  const toast = useToast()
  const router = useRouter()

  const [error, setError] = useState<ErrorType>({})
  const [isLoading, setIsLoading] = useState(false)
  const [loginDetails, setLoginDetails] = useState({
    emailPhone: !!email ? email : '',
    password: '',
  })

 /*  const { mutate: signInWithEmailPhoneAndPassword, isLoading: isCustomLoginLoading } = useMutation(
    ['signin-with-email-phone'],
    () =>
      signinUser({
        emailPhone: loginDetails.emailPhone,
        password: loginDetails.password,
      }),
    {
      onSuccess: (res) => {
        // setSuccessOpen(true)
        // #TODO: check profile status and based on that redirect them to home or kyc page and other conditions
        const token = res?.token
        const { permissions, access_level } = res?.user
        const permissionsAccessLevel = {
          permissions,
          access_level,
        }
        storage.setPermissionsAccessLevel(permissionsAccessLevel)
        router.push(`/auth/success?token=${token}`)
      },
      onError: (err: any) => {
        setError(err.data.error)
        if (err.data.error.message && err.data.error.message !== '') {
          let id = err?.data?.error?.message
          if (!toast.isActive(id)) {
            toast({
              id,
              title: err?.data?.error?.message,
              status: 'error',
              position: 'top-right',
            })
          }
        }
      },
    }
  ) */

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setError({})
    const target = e.target
    if (target)
      setLoginDetails({
        ...loginDetails,
        [target?.name]: target?.value,
      })
  }

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setIsLoading(true)
    try {
      //await signInWithEmailPhoneAndPassword()
      // const res = await signIn('credentials', {
      //   ...loginDetails,
      //   redirect: false,
      // })

      // if (res?.status == 401) {
      //   const error = JSON.parse(res?.error as string)
      //   toast({
      //     title: error?.message,
      //     status: 'error',
      //     duration: 5000,
      //     position: 'top-right',
      //   })
      //   setError({ message: `${error?.message}` })
      // }

      // if (res?.ok) {
      //   router.push('/home')
      // }
    } catch (error: any) {
      toast({
        title: error?.message,
        status: 'error',
        duration: 5000,
        position: 'top-right',
      })
    }

    setIsLoading(false)
  }

  const pathName = usePathname()

  useEffect(() => {
    const token = storage.getToken()

    if (token) {
      // router.push('/home')
    }
  }, [])

  return (
    <>
      <OpenGraph
        title='Incentiv | Sign In'
        description='Sign in to your Incentiv account'
        url={`${INCENTIV_URL}${pathName}`}
      />
      <EmailProvider>
        <PhoneProvider>
          <IsUSDProvider>
            <AppLayout type='auth'>
              <div className='flex flex-col w-3/4'>
                <h1 className='text-xl xl:text-2xl font-semibold text-gray'>
                  It’s good to have you back!
                </h1>
                <p className='text-gray-2 text-base'>Don’t have an account?<Link href='/signup'>
                  <span className='text-[#9383FD] ml-2 cursor-pointer'>Sign up</span>
                </Link>
                </p>
                <SignInForm />
              </div>

            </AppLayout>
          </IsUSDProvider>
        </PhoneProvider>
      </EmailProvider>
    </>
  )
}

export default Page
