import Head from 'next/head'
import React from 'react'

interface OpenGraphProps {
  title: string
  description: string
  type?: string
  image?: string
  url: string
}

const OpenGraph = ({
  title,
  description,
  type = 'website',
  image = 'https://storage.googleapis.com/incentiv-staging-public/email-assets/incentiv-black.svg',
  url,
}: OpenGraphProps) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name='description' content={description} />
      <meta property='og:title' content={title} />
      <meta property='og:description' content={description} />
      <meta property='og:type' content={type} />
      <meta property='og:image' content={image} />
      <meta property='og:url' content={url} />
    </Head>
  )
}

export default OpenGraph
