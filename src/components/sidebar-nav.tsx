'use client';

import React, { useState } from 'react';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import Image from 'next/image';
import { SIDENAV_ITEMS } from '@constants/constants';
import { SideNavItem } from '@interfaces/ISideBar';
import { Icon } from '@iconify/react';
import { Button } from "@/components/ui/button";
const SideNav = () => {
    // console.log('SIDENAV_ITEMS', SIDENAV_ITEMS)
    return (
        <div className=" md:w-1/5 bg-[#111] h-screen flex-1 fixed border-r border-[#212121] hidden md:flex">
            <div className="flex flex-col space-y-6 w-full">
                <div className='flex justify-around items-center'>

                    <Link
                        href="/"
                        className="flex flex-row space-x-3 items-center justify-center md:justify-start md:px-6  h-12 w-full mt-3 mb-2"
                    >
                        <Image src={'/static/images/logo/xlogo-black.svg'}
                            width={80} height={60} />
                    </Link>
                    <Button className='rounded-md w-[1.75rem] h-[1.7rem] p-1 justify-center items-center mr-[-0.875rem]'>
                        <Image src={'/static/images/client-dashboard/Arrow.svg'} width={15} height={15} />
                    </Button>
                </div>
                <div className="flex flex-col space-y-2  md:px-6 ">
                    {SIDENAV_ITEMS.map((item, idx) => {
                        //console.log('item', item)
                        return <MenuItem key={idx} item={item} />;
                    })}
                </div>
            </div>
        </div>
    );
};

export default SideNav;

const MenuItem = ({ item }: { item: SideNavItem }) => {
    const pathname = usePathname();
    const [subMenuOpen, setSubMenuOpen] = useState(false);
    const [isHovered, setIsHovered] = useState(false);
    const toggleSubMenu = () => {
        setSubMenuOpen(!subMenuOpen);
    };

    return (
        <div className="text-[#858585] hover:text-gray hover:bg-primary rounded-lg" onMouseEnter={() => {
            setIsHovered(!isHovered)
            console.log("entered", isHovered)
        }} onMouseLeave={() => {
            setIsHovered(!isHovered)
            console.log("exit", isHovered)
        }}>
            {item.submenu ? (
                <>
                    <button
                        onClick={toggleSubMenu}
                        className={`flex flex-row items-center p-2  hover-bg-zinc-100 w-full justify-between   ${pathname.includes(item.path) ? 'bg-primary rounded-lg text-gray' : ''
                            }`}
                    >
                        <div className="flex flex-row space-x-4 items-center  ">
                            {isHovered || pathname.includes(item.path) ? item.whiteIcon : item.icon}
                            <span className="font-semibold text-sm  flex   ">{item.title}</span>
                        </div>

                        <div className={`${subMenuOpen ? 'rotate-180' : ''} flex`}>
                            {isHovered ? <Image src={'/static/images/navigation-menu-icons/down-arrow-white.svg'} width={15} height={15} /> : <Image src={'/static/images/navigation-menu-icons/down-arrow.svg'} width={15} height={15} />}

                        </div>
                    </button>

                    {subMenuOpen && (
                        <div className="my-2 ml-12 flex flex-col space-y-4">
                            {item.subMenuItems?.map((subItem, idx) => {
                                return (
                                    <Link
                                        key={idx}
                                        href={subItem.path}
                                        className={`${subItem.path === pathname ? 'font-bold' : ''
                                            }`}
                                    >
                                        <span className="font-semibold text-sm  flex ">{subItem.title}</span>
                                    </Link>
                                );
                            })}
                        </div>
                    )}
                </>
            ) : (
                <Link
                    href={item.path}
                    className={`flex flex-row space-x-4 items-center p-2    ${item.path === pathname ? 'bg-primary rounded-lg text-gray' : ''
                        }`}
                >
                    {console.log("item", pathname)}
                    {isHovered || pathname.includes(item.path) ? item.whiteIcon : item.icon}
                    <span className="font-semibold text-sm flex ">{item.title}</span>
                </Link>
            )}
        </div>
    );
};
