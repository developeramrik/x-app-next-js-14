"use client";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
const AuthLayout: React.FC = ({ children }) => {
    return (
        <div className='flex w-screen min-h-screen bg-auth-new-gradient' >
            <div className='absolute left-10 top-6 z-10 '>
                <>
                    <Link href='/'>
                        <div className='cursor-pointer w-24 h-12 lg:w-28 lg:h-12 relative'>
                            <Image
                                src={'/static/images/logo/xlogo.svg'}
                                alt='incentiv'
                                layout='fill'
                            />
                        </div>
                    </Link>
                </>
            </div>
            <div className='w-7/12 relative min-h-screen'>
                <div className="absolute  inset-0 bg-auth-img bg-no-repeat bg-center -left-24 flex justify-center items-center xl:-left-16" style={{ top: "-400px" }}>
                    {
                        //absolute inset-0 bg-auth-img bg-no-repeat bg-primary -top-24 -left-24 flex justify-center items-center bg-cover bg-fixed bg-center sm:bg-left md:bg-right lg:bg-top xl:bg-bottom
                    }

                </div>
                <div className="flex flex-col gap-6 relative top-20 items-start justify-center left-24 mt-16">
                    <h1 className='text-xl xl:text-2xl font-semibold text-gray'>
                        Simplest way to manage clients
                    </h1>
                    <h3 className='text-gray'>The easiest way for financial professionals to efficiently <br /> handle and organize their client portfolios.</h3>
                    <div className='relative w-[200px] h-[100px] mt-4'>
                        <Image
                            src={'/static/images/auth-span-img.svg'}
                            alt='auth-span-img'
                            width={100}
                            height={100}

                            objectFit="contain"
                        />
                    </div>

                </div>
                <div className="mt-20 absolute bottom-0 right-0 bg-auth-footer w-full h-[60%] -mr-24" />
            </div>
            <div className='w-5/12 z-10 bg-[#111111] flex justify-center items-center'>{children}</div>
        </div>
    );
};

export default AuthLayout;