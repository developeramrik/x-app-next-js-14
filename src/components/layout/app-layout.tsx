import dynamic from 'next/dynamic'
import { ReactNode } from 'react'
const OnBoardingLayout = dynamic(() => import('./onboarding-layout'))
const AuthLayout = dynamic(() => import('./auth-layout'))
const DashboardLayout = dynamic(() => import('./dashboard-layout'))
interface LayoutProps {
  type: 'auth' | 'dashboard' | 'landing' | 'about-us' | 'onboarding'
  children: ReactNode
  page?: string
  currentUser?: any
  isLoggedIn?: boolean
}

const AppLayout = (props: LayoutProps) => {
  const { type, children, page, currentUser, isLoggedIn } = props
  switch (type) {
    case 'auth':
      return  <AuthLayout page={page}>{children}</AuthLayout>
    case 'onboarding':
      return <OnBoardingLayout>{children}</OnBoardingLayout>
    case 'dashboard':
      return <DashboardLayout>{children}</DashboardLayout>
    default:
      return <>{children}</>
  }
}

export default AppLayout
