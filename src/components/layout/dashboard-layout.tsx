import React, { ReactNode, useState } from 'react';

interface DashboardLayoutProps {
    children: ReactNode
    currentUser?: any
    isLoggedIn?: boolean
}
import SideNav from "@components/sidebar-nav";
const DashboardLayout: React.FC<DashboardLayoutProps> = (props: DashboardLayoutProps) => {
    const { children, currentUser, isLoggedIn } = props
    const [isSideNavOpen, setIsSideNavOpen] = useState(false)
    const sideNavTrigger = () => {
        setIsSideNavOpen(!isSideNavOpen)
    }
    return (
        <div className='w-screen h-screen flex'>
            {/* <div className='sm:hidden'>
                <Drawer open={isSideNavOpen} onOpenChange={sideNavTrigger}>
                    <DrawerTrigger asChild>
                        <Button variant="outline">Edit Profile</Button>
                    </DrawerTrigger>
                    <DrawerContent>
                        <DrawerHeader className="text-left">
                            <DrawerTitle>Edit profile</DrawerTitle>
                            <DrawerDescription>
                                Make changes to your profile here. Click save when you're done.
                            </DrawerDescription>
                        </DrawerHeader>
{/*                         <ProfileForm className="px-4" />
                         <DrawerFooter className="pt-2">
                            <DrawerClose asChild>
                                <Button variant="outline">Cancel</Button>
                            </DrawerClose>
                        </DrawerFooter>
                    </DrawerContent>
                </Drawer>
            </div> */}
            <div className="flex w-1/5">

                <SideNav />
            </div>
            <div className="flex w-4/5 flex-col">
                {/* dashboard header section*/}
                <div className="flex flex-col">
                    <div className="flex flex-row justify-between items-center bg-white p-4">
                        <div className="flex flex-row items-center">
                            <button onClick={sideNavTrigger} className="mr-4">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M4 6h16M4 12h16m-7 6h7"
                                    />
                                </svg>
                            </button>
                            <h1 className="text-2xl font-bold">Dashboard</h1>
                        </div>
                        <div className="flex flex-row items-center">
                            <div className="flex flex-row items-center">
                                <div className="h-10 w-10 rounded-full bg-gray-200"></div>
                                <p className="ml-2">{currentUser?.name}</p>
                            </div>
                        </div>
                    </div>
                </div>
                {/* dashboard content section*/}
                {children}
            </div>
        </div>
    );
};

export default DashboardLayout;