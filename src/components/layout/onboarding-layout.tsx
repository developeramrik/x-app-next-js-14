"use client";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
const OnBoardingLayout: React.FC = ({ children }) => {
    return (
        <div className='flex flex-col justify-center items-center w-screen min-h-screen bg-onboarding-bg' >
            <div className='absolute left-10 top-6 z-10 '>
                <>
                    <Link href='/'>
                        <div className='cursor-pointer w-24 h-12 lg:w-28 lg:h-12 relative'>
                            <Image
                                src={'/static/images/logo/xlogo-black.svg'}
                                alt='incentiv'
                                layout='fill'
                            />
                        </div>
                    </Link>
                </>
            </div>
            {children}
        </div>
    );
};

export default OnBoardingLayout;