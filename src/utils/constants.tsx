import { Icon } from '@iconify/react';
import { SideNavItem } from '@interfaces/ISideBar';
import Image from 'next/image';
export const ACCESS_TOKEN_KEY = 'token'
export const PERMISSIONS_ACCESS_LEVEL = 'permissions_access_level'
export const USER_KEY = 'user'
export const MINIMUM_INVESTMENT = 100000
export const MAXIMUM_INVESTMENT = 999999999

export const INCENTIV_URL =
  (process.env.NEXT_PUBLIC_INCENTIV_URL as string) || 'https://incentiv.finance/'



  
export const SIDENAV_ITEMS: SideNavItem[] = [
    {
      title: 'Home',
      section: 'home',
      path: '/home',
      icon: <Image src={'/static/images/navigation-menu-icons/home.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/home-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Marketplace',
      section: 'management',
      path: '/projects',
      icon: <Image src={'/static/images/navigation-menu-icons/marketplace.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/marketplace-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Clients',
      section: 'management',
      path: '/clients',
      icon: <Image src={'/static/images/navigation-menu-icons/clients.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/clients-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Commitments',
      section: 'management',
      path: '/commitments',
      icon: <Image src={'/static/images/navigation-menu-icons/commitments.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/commitments-white.svg'} width={24} height={24} />,
      submenu: true,
      subMenuItems: [
        { title: 'Account', path: '/settings/account' },
        { title: 'Privacy', path: '/settings/privacy' },
      ],
    },
    {
      title: 'Referrals',
      section: 'management',
      path: '/referrals',
      icon: <Image src={'/static/images/navigation-menu-icons/referrals.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/referrals-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Account',
      section: 'others',
      path: '/account',
      icon: <Image src={'/static/images/navigation-menu-icons/account.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/account-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Settings',
      section: 'others',
      submenu: true,
      path: '/settings',
      icon: <Image src={'/static/images/navigation-menu-icons/settings.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/settings-white.svg'} width={24} height={24} />,
    },
    {
      title: 'Notifications',
      section: 'others',
      path: '/notifications',
      icon: <Image src={'/static/images/navigation-menu-icons/notification.svg'} width={24} height={24} />,
      whiteIcon: <Image src={'/static/images/navigation-menu-icons/notification-white.svg'} width={24} height={24} />,
    }
  ];
  