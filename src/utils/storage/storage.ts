"use client";
import { ACCESS_TOKEN_KEY, PERMISSIONS_ACCESS_LEVEL } from '../constants'

const storage = {
  setPermissionsAccessLevel: (permissions: {}) => {
    const item = JSON.stringify(permissions)
    localStorage.setItem(PERMISSIONS_ACCESS_LEVEL, item)
  },
  getPermissionsAccessLevel: () => {
    const itemStr = localStorage.getItem(PERMISSIONS_ACCESS_LEVEL)
    if (!itemStr) {
      return null
    }
    const permissions = JSON.parse(itemStr)
    return permissions
  },
  clearPermissionsAccessLevel: () => {
    localStorage.removeItem(PERMISSIONS_ACCESS_LEVEL)
  },
  getToken: () => {
    const itemStr = localStorage.getItem(ACCESS_TOKEN_KEY)
    if (!itemStr) {
      return null
    }
    const token = JSON.parse(itemStr)
    return token
  },
  setToken: (token: string) => {
    const item = JSON.stringify(token)
    localStorage.setItem(ACCESS_TOKEN_KEY, item)
  },
  clearToken: () => {
    localStorage.removeItem(ACCESS_TOKEN_KEY)
  },
}

export default storage
