"use client";
import { USER_KEY } from '../constants'

const userStorage = {
  getUser: () => {
    const userStr = localStorage.getItem(USER_KEY)
    if (!userStr) {
      return null
    } else {
      const user = JSON.parse(userStr)
      return user
    }
  },
  setUser: (userToSet: any) => {
    const user = JSON.stringify(userToSet)
    localStorage.setItem(USER_KEY, user)
  },
  clearUser: () => {
    localStorage.removeItem(USER_KEY)
  },
}

export default userStorage
