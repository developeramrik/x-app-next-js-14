import * as React from 'react'
import Image from 'next/image'
import { cn } from '@/lib/utils'
import { cva, VariantProps } from 'class-variance-authority'
import { set } from 'nprogress'

export const inputVariants = cva('flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus:outline-none focus:ring-[0.75px] focus:ring-primary disabled:cursor-not-allowed disabled:opacity-50'
  , {
    variants: {
      variant: {
        default: 'border border-input bg-placeholder-color text-[#555555] border-[#444444] text-muted-foreground placeholder: text-[#555555]',
        file: 'border-dashed border-2 border-[#444] rounded-md w-full h-12 text-gray text-sm font-medium text-center ',
        dark: 'border border-[#3D3D3D] bg-[#1A1A1A] text-muted-foreground',
        password: ' border border-input bg-placeholder-color px-3 py-2 text-[#555555]  border-[#444444] text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground  disabled:cursor-not-allowed disabled:opacity-50',
      },
    },
    defaultVariants: {
      variant: 'default',
    },
  }
)




export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement>,
  VariantProps<typeof inputVariants> {
  asChild?: boolean
  onFileUpload?: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, variant, type, ...props }, ref) => {
    //console.log('input.tsx', 'Input', type, props, ref, variant, className)
    const [showPassword, setShowPassword] = React.useState(false)

    if (type === 'password') {
      return (
        <>
          <div className='flex flex-row mb-3 w-full relative justify-center items-center'>
            <input
              type={showPassword ? "text" : "password"}
              className={cn(inputVariants({ variant, className }))}
              ref={ref}
              {...props}
            />
            {
              type === 'password' &&
              <Image
                src={
                  showPassword
                    ? '/static/images/hidden-icon.svg'
                    : '/static/images/eye-icon.svg'
                }
                alt=''
                width={16}
                height={16}
                className='cursor-pointer absolute right-0 m-4'
                onClick={(e) => {
                  console.log('input.tsx', 'onClick', showPassword)
                  setShowPassword(!showPassword)
                }}
              />
            }
          </div>
        </>)
    }
    else if (type === 'bulk-file-upload-client') {
      //console.log('input.tsx', 'bulk-file-upload-client', type, props, ref, variant, className)
      return (
        <>
          <div className='flex bg-[#212121] flex-row mb-3 mt-3 w-full min-h-[8.75rem] relative justify-center items-center border-dashed border-2 border-[#444] rounded-md text-gray text-sm font-medium text-center ' >
            <input
              type={'file'}
              className={`opacity-0 ${cn(inputVariants({ variant, className }))}`}
              ref={ref}
              placeholder={props.placeholder || 'Enter text here'}
              {...props}
              title=' '
              onChange={props.onFileUpload}
              id='bulk-import'
            />
            <label htmlFor="bulk-import" className="absolute left-0 right-0 top-0 bottom-0 flex justify-center items-start cursor-pointer w-full h-full">
              <div className='flex flex-col justify-start items-center mt-4 gap-2'>
                <Image src={'/static/images/client-dashboard/file-upload.svg'} alt='file-upload' width={80} height={80} />
                <span className='text-xs text-[#DDD] font-medium mt-2'>Drag & drop or <span className='text-primary'>Choose file</span> to upload</span>
                <span className='text-text-new-Body text-xxs font-normal'>Maximum file size 20 MB.</span>
              </div>
            </label>
          </div>
        </>
      )
    }
    else {
      return (
        <>
          <div className='flex flex-row mb-3 w-full relative justify-center items-center'>
            <input
              type={type}
              className={cn(inputVariants({ variant, className }))}
              ref={ref}
              {...props}
            />
          </div>
        </>

      )
    }

  }
)
Input.displayName = 'Input'
export { Input }
