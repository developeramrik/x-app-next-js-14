import { createContext, useState } from 'react'

const PhoneContext = createContext({
  phone: '',
  addPhone: (phone: string) => {},
})

export const PhoneProvider = ({ children }: any) => {
  const [phone, setPhone] = useState('')

  const addPhone = (phone: string) => {
    setPhone(phone)
  }

  return <PhoneContext.Provider value={{ phone, addPhone }}>{children}</PhoneContext.Provider>
}

export default PhoneContext
