"use client"
import React, { createContext, useState } from 'react'

interface IsUSDProviderProps {
  children: React.ReactNode
}
interface IUSDContext {
  isUSD: boolean
  toggleIsUSD: () => void
}

const defaultState: IUSDContext = {
  isUSD: false,
  toggleIsUSD: () => {},
}

export const IsUSDContext = createContext<IUSDContext>(defaultState)

export const IsUSDProvider = ({ children }: IsUSDProviderProps) => {
  const [isUSD, setIsUSD] = useState<boolean>(defaultState.isUSD)

  const toggleIsUSD = () => {
    setIsUSD((prev) => !prev)
  }

  const contextValue: IUSDContext = {
    isUSD,
    toggleIsUSD,
  }

  return <IsUSDContext.Provider value={contextValue}>{children}</IsUSDContext.Provider>
}
