import { createContext, useState } from 'react'

const EmailContext = createContext({
  email: '',
  addEmail: (email: string) => {},
})

export const EmailProvider = ({ children }: any) => {
  const [email, setEmail] = useState('')

  const addEmail = (email: string) => {
    setEmail(email)
  }

  return <EmailContext.Provider value={{ email, addEmail }}>{children}</EmailContext.Provider>
}

export default EmailContext
