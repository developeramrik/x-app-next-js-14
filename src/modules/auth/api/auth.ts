import { get, post } from '@lib/axios-instance'
import axios from 'axios'
import { OnboardingType } from '@interfaces/OnboardingType'
import {
  ForgotPassword,
  InstitutionManagerProfile,
  LoginDetails,
  OtpDetails,
  ProfileDetails,
  ResendOtpDetails,
  ResetPassword,
  SignupDetails,
} from '../interfaces'

export const signinWithGoogle = async () => {
  const response = await get(`/api/auth/google`)

  return response.data.data
}

export const signoutUser = async () => {
  const response = await get(`/api/auth/logout`)
  return response.data.data
}

export const signinUser = async (payload: LoginDetails) => {
  const response = await post(`/api/auth/login`, payload)
  return response.data.data
}

export const signupUser = async (payload: SignupDetails) => {
  const response = await post(`/api/auth/user/register`, payload)
  return response.data
}

export const checkAuthToken = async () => {
  const response = await get('/api/auth/check-auth-token')
  return response.data.data
}

export const sendOtp = async (payload: ResendOtpDetails) => {
  const response = await post(`/api/auth/user/send-otp`, payload)
  return response
}

export const verifyOtp = async (payload: OtpDetails) => {
  const response = await post(`/api/auth/user/verify-otp`, payload)
  return response
}
export const ResendOtp = async (payload: ResendOtpDetails) => {
  const response = await post(`/api/auth/user/resend-otp`, payload)
  return response?.data
}
export const completeProfile = async (payload: ProfileDetails) => {
  const response = await post(`/api/user/complete-profile`, payload)
  return response.data.data
}

export const completeInstitutionManagerProfile = async (
  payload: InstitutionManagerProfile,
) => {
  const response = await post(
    `/api/user/complete-profile?onboardingType=${OnboardingType.institution}`,
    payload,
  )
  return response.data.data
}

export const resetPassword = async (
  payload: ResetPassword,
  resetToken: string,
) => {
  const response = await axios.put(
    `${process.env.NEXT_PUBLIC_DB_BASE_URL}/api/auth/reset-password`,
    payload,
    {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        Authorization: `Bearer ${resetToken}`,
      },
    },
  )
  return response.data.data
}

export const forgotPassword = async (payload: ForgotPassword) => {
  const response = await post(`/api/auth/forgot-password`, payload)
  return response.data.data
}

export const verifyToken = async (token: string | string[] | undefined) => {
  const response = await get(`/api/auth/verify-token?token=${token}`)
  return response.data.data
}

export const verifyUserToken = async (token: string | string[] | undefined) => {
  const response = await get(`/api/user/verify/${token}`)
  return response.data.data
}

export const sendVerificationEmail = async (payload: any) => {
  const response = await post(`/api/auth/send-verification-email`, payload)
  return response.data.data
}

export const resendVerificationEmail = async (payload: any) => {
  const response = await post(`/api/auth/resend-verification-email`, payload)
  return response.data.data
}

export const verifyEmail = async (token: string | string[] | undefined) => {
  const response = await get(`/api/auth/verify-email?token=${token}`)
  return response.data.data
}
